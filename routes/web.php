<?php

use Illuminate\Support\Facades\Route;

/*
 * Home
 */

Route::get('/', [
    'uses' => '\App\Http\Controllers\HomeController@index',
    'as' => 'home',
]);

/*
 * Alert
 */

Route::get('/alert', function () {
    return redirect()->route('home')->with('info', 'You have signed up!');
});

/*
 * Auth
 */

Route::get('/signup',[
    'uses' => '\App\Http\Controllers\AuthController@getSignup',
    'as' => 'auth.signup',
]);

Route::post('/signup',[
    'uses' => '\App\Http\Controllers\AuthController@postSignup',
]);

Route::get('/signin',[
    'uses' => '\App\Http\Controllers\AuthController@getSignin',
    'as' => 'auth.signin',
]);

Route::post('/signin',[
    'uses' => '\App\Http\Controllers\AuthController@postSignin',
]);
