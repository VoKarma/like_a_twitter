<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function getSignup()
    {
        return view('auth.signup');
    }

    public function postSignup(Request $request)
    {

        $this->validate($request, [
            'username' => 'required|unique:users|alpha_dash|max:25',
            'email' => 'required|unique:users|email|max:255',
            'password' => 'required|min:6',
            'confirm_password' => 'required|same:password|min:6',
        ]);

        dd('All Ok. Sign up...');

        User::create([
            'email' => $request -> input('email'),
            'username' => $request -> input('username'),
            'password' => $request -> input('password'),
        ]);

        return redirect()
            ->route('home')
            ->with('info', 'Your account has been created and you can sign in.');
    }

    public function getSignin()
    {
        return view('auth.signin');
    }

    public function postSignin()
    {

    }
}
