@if (Session::has('into'))
    <div class="alert alert-info" role="alert">
        {{ Session::get('info') }}
    </div>
@endif
