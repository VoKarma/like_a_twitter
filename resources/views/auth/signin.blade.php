@extends('templates.default')

@section('content')
    <div class="form-group">
        <h3>Sing In</h3>
    </div>
    <div class="row">
        <div class="col-lg-6">
            <form class="form-vertical" role="form" method="post" action="#">
                <div class="form-group">
                    <label for="email" class="control-label">Email</label>
                    <input type="email" name="email" class="form-control" id="email">
                </div>
                <div class="form-group">
                    <label for="password" class="control-label">Password</label>
                    <input type="password" name="password" class="form-control" id="password">
                </div>
                <div class="checkbox">
                    <label>
                        <input type="checkbox" name="remember">Remember me
                    </label>
                </div>
                <div class="form-group">
                    <button type="submit" class="btn btn-default">Sign in</button>
                </div>
            </form>
        </div>
    </div>

@stop
