@extends('templates.default')

@section('content')

    <div class="form-group">
        <h3>Sign Up</h3>
    </div>
    <div class="row">
        <div class="col-lg-6">
            <form class="form-vertical" role="form" method="post" action="{{ route('auth.signup') }}">

                <div class="form-group{{ $errors->has('username') ? ' has-error' : '' }}">
                    <label for="username" class="control-label">Username:</label>
                    <input type="text" name="username" class="form-control" id="username" value="{{ Request::old( 'username' ?: '') }}">
                    @if($errors->has('email'))
                        <span class="help-block">{{ $errors->first('username') }}</span>
                    @endif
                </div>
                <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                    <label for="email" class="control-label">Your email address:</label>
                    <input type="email" name="email" id="email" class="form-control" value=" {{ Request::old( 'email' ?: '') }}">
                    @if($errors->has('email'))
                        <span class="help-block">{{ $errors->first('email') }}</span>
                    @endif
                </div>
{{-- password --}}
                <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                    <label for="password" class="control-label">Password:</label>
                    <input type="password" name="password" class="form-control" id="password">
                    @if($errors->has('email'))
                        <span class="help-block">{{ $errors->first('password') }}</span>
                    @endif
                </div>
                <div class="form-group{{ $errors->has('confirm_password') ? ' has-error' : '' }}">
                    <label for="confirm_password" class="control-label">Confirm Password:</label>
                    <input type="password" name="confirm_password" class="form-control" id="confirm_password">
                    @if($errors->has('email'))
                        <span class="help-block">{{ $errors->first('confirm_password') }}</span>
                    @endif
                </div>
{{-- agree  --}}
                <div class="form-group">
                    <label for="agree">
                        <input type="checkbox" name="agree" id="agree" value="yes"/> I agree
                        with the
                        <a href="#" title="term of services">term of services</a>
                    </label>
                </div>
                <div class="form-group">
                    <button type="submit" class="btn btn-default">Sign up</button>
                </div>
                    <div class="form-group">
                        <footer>Already a member? <a href="signin.blade.php">Sign in here</a></footer>
                    </div>
                <input type="hidden" name="_token" value="{{ Session::token() }}">
            </form>
        </div>
    </div>
@stop
